set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'dracula/vim', {'name': 'dracula'}

call vundle#end()

filetype plugin indent on

"" General
set number relativenumber
set ruler
set showcmd
set wrap
set ttyfast
set wildmenu

"" Color
set termguicolors
set t_8f=[38;2;%lu;%lu;%lum
set t_8b=[48;2;%lu;%lu;%lum
colorscheme dracula
syntax enable
hi Normal guibg=NONE ctermbg=NONE

"" Tabs
set autoindent
set expandtab
set softtabstop=4
set tabstop=4
set shiftwidth=4

"" Search
set incsearch
set hlsearch
nnoremap <leader><space> :nohlsearch<CR>
