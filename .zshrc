#
# ~/.zshrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# PATH
export PATH="$HOME/.emacs.d/bin:$PATH"

# Prompt
PROMPT='%B %F{117}%1~%f %(?.%F{green}$%f.%F{red}$%f)%b '
RPROMPT='%F{117}%~%f'

precmd() { eval "$PROMPT_COMMAND" }

# Set VI mode
bindkey -v

# Plugins
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# zsh-autosuggestions
bindkey '^ ' autosuggest-accept

# zsh-syntax-highlighting
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[path]='fg=cyan'


# Aliases
alias ls='ls -lh --color=auto --group-directories-first'
alias la='ls -Alh --color=auto --group-directories-first'

alias dotgit='git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias pacinstall='sudo pacman -S'
alias pacremove='sudo pacman -Rs'
alias pacupdate='sudo pacman -Syu'
alias pacsearch='pacman -Ss'
alias pacquery='pacman -Q'
alias pacorphans='pacman -Qdt'

alias emacs="emacsclient -c -a 'emacs'"

alias puke="clear && $HOME/color-scripts/pukeskull"
alias draw="clear && $HOME/color-scripts/ghosts"

# ghcup
# [ -f "/home/jack/.ghcup/env" ] && source "/home/jack/.ghcup/env" # ghcup-env

# Color script
$HOME/color-scripts/ghosts
