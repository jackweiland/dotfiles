import System.IO

import XMonad

import XMonad.Actions.CycleWS
import XMonad.Actions.UpdatePointer
import XMonad.Actions.WindowBringer

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.InsertPosition
import XMonad.Hooks.ManageDocks

import XMonad.Layout.Decoration
import XMonad.Layout.DwmStyle
import XMonad.Layout.NoFrillsDecoration
import XMonad.Layout.Grid
import XMonad.Layout.IndependentScreens(countScreens)
import XMonad.Layout.NoBorders
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Renamed
import XMonad.Layout.Simplest
import XMonad.Layout.SubLayouts
import XMonad.Layout.Tabbed
import XMonad.Layout.WindowNavigation

import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.SpawnOnce
import qualified XMonad.Util.Dmenu as D

import qualified XMonad.StackSet as SS

import SideDecorations

-- Mod key
myModMask       = mod4Mask

-- Applications
myTerminal      = "alacritty"
myBrowser       = "firefox"
myEditor        = "emacsclient -c -a 'emacs'"
myScreenshot    = "maim"

-- Border width
myBorderWidth   = 2

-- Colors
dracBackground  = "#282a36"
dracCurrentLine = "#44475a"
dracForeground  = "#f8f8f2"
dracComment     = "#6272a4"
dracCyan        = "#8be9fd"
dracGreen       = "#50fa7b"
dracOrange      = "#ffb86c"
dracPink        = "#ff79c6"
dracPurple      = "#bd93f9"
dracRed         = "#ff5555"
dracYellow      = "#f1fa8c"

-- Font
myFont size     = "xft:DejavuSansMono:size=" ++ show size

-- Workspaces
myWorkspaces    = ["home", "www", "doc", "dev", "sys", "mus" ,"chat", "vid", "misc"]

-- Startup hook
myStartupHook = do
  spawnOnce "picom --experimental-backends &"
  spawnOnce "nitrogen --restore &"
  spawnOnce "/usr/bin/emacs --daemon &"
  spawnOnce "dunst &"

-- Layout hook
myDecoTheme = def
  { activeColor = dracPurple
  , inactiveColor = dracBackground
  , activeBorderColor = dracPurple
  , inactiveBorderColor = dracBackground
  , decoWidth = 3
  , decoHeight = 3
  , fontName = myFont 9
  }

myTabTheme = def
  { activeColor          = dracCurrentLine
  , inactiveColor        = dracCurrentLine
  , activeBorderColor    = dracCyan
  , inactiveBorderColor  = dracBackground
  , activeTextColor      = dracCyan
  , inactiveTextColor    = dracForeground
  , fontName             = myFont 9
  }

myDeco :: Eq a => l a -> ModifiedLayout (Decoration SideDecoration DefaultShrinker) l a
myDeco = decoration shrinkText myDecoTheme (SideDecoration L)

tall    = renamed [Replace "tall"]
          $ noBorders
          $ myDeco
          $ windowNavigation
          $ addTabs shrinkText myTabTheme
          $ subLayout [] Simplest
          $ Tall 1 0.03 0.5

grid    = renamed [Replace "grid"]
          $ noBorders
          $ myDeco
          $ windowNavigation
          $ addTabs shrinkText myTabTheme
          $ subLayout [] Simplest
          $ GridRatio (4/3)

tabs    = renamed [Replace "tabs"]
          $ noBorders
          $ tabbed shrinkText myTabTheme

myLayoutHook  = avoidStruts
                $ onWorkspace "www" (tabs ||| tall)
                $ onWorkspaces ["mus", "chat"] tabs
                $ onWorkspace "vid" (noBorders Full)
                $ tall ||| grid ||| tabs

-- Key bindings
myWBConfig prompt = def { menuCommand   = "./.config/dmscripts/dm-basic"
                        , menuArgs      = [prompt]
                        }

myKeys =
  -- Applications
  [ ((myModMask,                 xK_b),       spawn myBrowser)
  , ((myModMask,                 xK_s),       spawn $ myScreenshot ++ " -s | xclip -selection clipboard -t image/png")
  , ((myModMask .|. shiftMask,   xK_s),       spawn $ myScreenshot ++ " -s ~/Pictures/screenshots/$(date +%s).png")
  , ((myModMask,                 xK_Return),  spawn myTerminal)
  , ((myModMask,                 xK_v),       spawn myEditor)

  -- Move windows
  , ((myModMask .|. shiftMask,   xK_m),       windows SS.swapMaster)
  , ((myModMask,                 xK_f),       sendMessage ToggleStruts)
  , ((myModMask .|. controlMask, xK_h),       sendMessage $ pushGroup L)
  , ((myModMask .|. controlMask, xK_j),       sendMessage $ pushGroup D)
  , ((myModMask .|. controlMask, xK_k),       sendMessage $ pushGroup U)
  , ((myModMask .|. controlMask, xK_l),       sendMessage $ pushGroup R)
  , ((myModMask .|. controlMask, xK_u),       withFocused (sendMessage . UnMerge))
  , ((myModMask .|. controlMask, xK_period),  onGroup SS.focusUp')
  , ((myModMask .|. controlMask, xK_comma),   onGroup SS.focusDown')

  -- Dmenu
  , ((myModMask,                 xK_p),       spawn "bash ~/.config/dmscripts/dm-main")
  , ((myModMask .|. shiftMask,   xK_p),       spawn "bash ~/.config/dmscripts/dm-hub")
  , ((0,                         xK_Delete),  spawn "bash ~/.config/dmscripts/dm-logout")

  -- Navigate workspaces
  , ((myModMask,                 xK_slash),   gotoMenuConfig $ myWBConfig "goto:")
  , ((myModMask .|. shiftMask,   xK_slash),   bringMenuConfig $ myWBConfig "grab:")
  , ((myModMask,                 xK_space),   D.menuArgs "./.config/dmscripts/dm-basic" ["swap:"] myWorkspaces >>= windows . SS.greedyView)
  --, ((myModMask .|. shiftMask,   xK_space),   D.menuArgs "./.config/dmscripts/dm-basic" ["move:"] myWorkspaces >>= windows . (\ws -> SS.view ws . SS.shift ws))
  , ((myModMask,                 xK_apostrophe), toggleWS)

  -- Layouts
  , ((myModMask,                 xK_Escape),  sendMessage NextLayout)

  -- Dunstctl
  , ((myModMask,                 xK_x),       spawn "dunstctl history-pop")
  , ((myModMask .|. shiftMask,   xK_x),       spawn "dunstctl context")
  , ((myModMask .|. controlMask, xK_x),       spawn "dunstctl close-all")

  -- Multimedia keys
  , ((0,                         0x1008FF12), spawn "bash ~/.config/dunst/changeVolume toggle")
  , ((0,                         0x1008FF11), spawn "bash ~/.config/dunst/changeVolume 5%- unmute")
  , ((0,                         0x1008FF13), spawn "bash ~/.config/dunst/changeVolume 5%+ unmute")
  , ((0,                         0x1008FF16), spawn "bash ~/.config/dunst/changeTrack -v")
  , ((0,                         0x1008FF14), spawn "bash ~/.config/dunst/changeTrack -p")
  , ((0,                         0x1008FF17), spawn "bash ~/.config/dunst/changeTrack -n")
  , ((0,                         0x1008FF1B), spawn "bash ~/.config/dmscripts/dm-main")
  , ((0,                         0x1008FF02), spawn "bash ~/.config/dunst/changeBrightness +10%")
  , ((0,                         0x1008FF03), spawn "bash ~/.config/dunst/changeBrightness 10%-")
  ]

-- Log hook
myLogHook xbar = dynamicLogWithPP xmobarPP
  { ppOutput          = xbar
  , ppCurrent         = xmobarColor dracCyan    "" . wrap ("<box type=Bottom width=2 mb=2 color=" ++ dracCyan ++ ">") "</box>"
  , ppVisible         = xmobarColor dracCyan    "" . wrap "" ""
  , ppHiddenNoWindows = xmobarColor dracPurple  "" . wrap "" ""
  , ppHidden          = xmobarColor dracPink    "" . wrap "" ""
  , ppUrgent          = xmobarColor dracRed     "" . wrap "" ""
  , ppTitle           = xmobarColor dracCyan    "" . shorten 95
  , ppWsSep           = "  "
  , ppSep             = "<fc=" ++ dracComment ++ "> | </fc>"
  } >> updatePointer (0.25, 0.25) (0.25, 0.25)

myDefaults xbar = def
  { modMask            = myModMask
  , terminal           = myTerminal
  , workspaces         = myWorkspaces

  -- Window properties
  , normalBorderColor  = dracBackground
  , focusedBorderColor = dracCyan
  , borderWidth        = myBorderWidth

  -- Hooks
  , startupHook        = myStartupHook
  , manageHook         = insertPosition End Newer
  , logHook            = myLogHook xbar
  , layoutHook         = myLayoutHook
  } `additionalKeys` myKeys

-- Main
main = do
  nScreens <- countScreens
  if nScreens == 1
    then do
      xmproc0 <- spawnPipe "xmobar -x 0 ~/.config/xmobar/xmobarrc0"
      xmonad $ docks $ myDefaults ( hPutStrLn xmproc0 )
    else do
      xmproc0 <- spawnPipe "xmobar -x 0 ~/.config/xmobar/xmobarrc0"
      xmproc1 <- spawnPipe "xmobar -x 1 ~/.config/xmobar/xmobarrc1"
      xmonad $ docks $ myDefaults (\x -> hPutStrLn xmproc0 x >> hPutStrLn xmproc1 x)
